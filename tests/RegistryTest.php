<?php

    use PHPUnit\Framework\TestCase;
    use davidmaes\mongodb\Registry;
    use davidmaes\mongodb\MongoDB;

    class RegistryTest extends TestCase
    {
        /**
         * Tests the get method for null when a given key isn't set.
         */
        public function test_get_invalidKey()
        {
            $this->assertNull(Registry::get('this_key_does_not_exist'));
        }

        /**
         * Tests the get method for to give back the proper MongoDB instance
         */
        public function test_get_validKey()
        {
            $key = 'test';
            $value = new MongoDB();

            Registry::set($key, $value);

            $this->assertEquals($value, Registry::get($key));
        }

        public function test_load_validKey()
        {
            $config = new stdClass();
            $config->default = $this->createInstance('localhost', 27017);
            $config->test = $this->createInstance('localhost', 27017);

            $path = 'tests/mongodb.json';

            $this->writeConfig($path, $config);

            Registry::load($path);

            $this->assertDatabaseConfig($config->default, Registry::get('default'));
            $this->assertNotNull(Registry::get('test'));

            $this->unlinkConfig($path);
        }

        /**
         * Creates a config object from the given paramaters.
         *
         * @param string $host
         * @param int $port
         *
         * @return stdClass A new config object.
         */
        private function createInstance(string $host, int $port)
        {
            $config = new stdClass();
            $config->host = $host;
            $config->port = $port;

            return $config;
        }

        /**
         * Writes to a temporary config file.
         *
         * @param string $path
         * @param stdClass $config
         */
        private function writeConfig(string $path, stdClass $config)
        {
            file_put_contents($path, json_encode($config, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
        }

        /**
         * Asserts whether a MongoDB instance contains the values of a given config file.
         *
         * @param stdClass $expected The config as the source of truth.
         * @param MongoDB $actual The MongoDB instance that should have the same values.
         */
        private function assertDatabaseConfig(stdClass $expected, MongoDB $actual)
        {
            $this->assertEquals($expected->host, $actual->getHost());
            $this->assertEquals($expected->port, $actual->getPort());
        }

        /**
         * Removes the temporary config file.
         *
         * @param string $path
         */
        private function unlinkConfig(string $path)
        {
            unlink($path);
        }
    }
