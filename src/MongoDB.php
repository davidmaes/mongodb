<?php

    namespace davidmaes\mongodb;

    use MongoDB\Driver\Manager;
    use UnexpectedValueException;

    class MongoDB
    {
        /**
         * @var string MongoDB host.
         */
        private $host;

        /**
         * @var int MongoDB port
         */
        private $port;

        /**
         * @var Manager The actual MongoDB manager that handles our work.
         */
        private $manager;

        public function __construct()
        {
            $this->host = 'localhost';
            $this->port = '27017';
        }

        /**
         * Returns the host for the MongoDB instance.
         *
         * @return string The host for the MongoDB instance.
         */
        public function getHost()
        {
            return $this->host;
        }

        /**
         * Sets the host for the MongoDB instance.
         *
         * @param string $host The host for the MongoDB instance.
         *
         * @throws UnexpectedValueException Throws exception when this field is set while being connected.
         */
        public function setHost(string $host)
        {
            if ($this->isConnected()) {
                throw new UnexpectedValueException('Cannot set the host while being connected');
            }

            $this->host = $host;
        }

        /**
         * Returns whether this instance is connected or not.
         *
         * @return bool True if the instance is connected, false if it isn't.
         */
        public function isConnected()
        {
            return !!$this->manager;
        }

        /**
         * Returns the port number for the MongoDB instance.
         *
         * @return int The port number for the MongoDB instance.
         */
        public function getPort()
        {
            return $this->port;
        }

        /**
         * Sets the port number for the MongoDB instance.
         *
         * @param int $port The port number for the MongoDB instance.
         *
         * @throws UnexpectedValueException Throws exception when this field is set while being connected.
         */
        public function setPort($port)
        {
            if ($this->isConnected()) {
                throw new UnexpectedValueException('Cannot change the port field while being connected');
            }

            $this->port = $port;
        }

        /**
         * Connects to MongoDB.
         */
        public function connect()
        {
            $this->manager = new Manager('mongodb://' . $this->host . ':' . $this->port);
        }

        /**
         * Returns a collection to execute queries on.
         *
         * @param string $database The database that contains the collection.
         * @param string $collection The collection to execute queries on.
         * @return Collection An object that gives simple CRUD support for MongoDB collections.
         */
        public function getCollection(string $database, string $collection)
        {
            if (!$this->isConnected()) {
                $this->connect();
            }

            return new Collection($this->manager, $database, $collection);
        }
    }
