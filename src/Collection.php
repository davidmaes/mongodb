<?php

    namespace davidmaes\mongodb;

    use MongoDB\Driver\BulkWrite;
    use MongoDB\Driver\Manager;
    use MongoDB\Driver\Query;
    use stdClass;

    class Collection
    {
        /**
         * @var Manager The actual MongoDB manager that handles our work.
         */
        private $manager;

        /**
         * @var string MongoDB database
         */
        private $database;

        /**
         * @var string The database and collection currently working with.
         */
        private $collection;

        public function __construct(Manager $manager, string $database, string $collection)
        {
            $this->manager = $manager;
            $this->database = $database;
            $this->collection = $collection;
        }

        /**
         * Selects a document from the database/collection we are connected to. Note that we encode and decode the
         * result before returning it. This is due to the fact that the current MongoDB driver in PHP will always return
         * associative arrays. We don't want this. Maybe we can change this in the future to optimize it.
         *
         * @param int $id The id of the document we are fetching.
         * @return stdClass A document in object format, or null if there is none.
         */
        public function select(int $id)
        {
            $cursor = $this->manager->executeQuery(
                $this->getQueryContext(),
                new Query(['_id' => $id])
            );

            foreach ($cursor as $document) // We only want the first result.
            {
                return json_decode(json_encode($document));
            }

            return null;
        }

        /**
         * Concatenates the database and collection field which we need to actually execute queries on.
         *
         * @return string The simple concatenation of the database and collection field.
         */
        private function getQueryContext()
        {
            return $this->database . '.' . $this->collection;
        }

        /**
         * Insert a document into the database/collection we are connected to.
         *
         * @param $document stdClass The document to be inserted.
         */
        public function insert(stdClass $document)
        {
            $bulkWrite = new BulkWrite();
            $bulkWrite->insert($document);

            $this->manager->executeBulkWrite(
                $this->getQueryContext(),
                $bulkWrite
            );
        }

        /**
         * Insert a document into the database/collection we are connected to.
         *
         * @param stdClass $document The document to be inserted.
         */
        public function update(stdClass $document)
        {
            $bulkWrite = new BulkWrite();
            $bulkWrite->update(['_id' => $document->_id], $document);

            $this->manager->executeBulkWrite(
                $this->getQueryContext(),
                $bulkWrite
            );
        }

        /**
         * Removes a single document by Id from the database/collection we are connected to.
         *
         * @param $id int The id of the document to be removed.
         */
        public function remove(int $id)
        {
            $bulkWrite = new BulkWrite();
            $bulkWrite->delete(['_id' => $id]);

            $this->manager->executeBulkWrite(
                $this->getQueryContext(),
                $bulkWrite
            );
        }
    }