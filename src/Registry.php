<?php

    namespace davidmaes\mongodb;

    use davidmaes\config\Config;

    class Registry
    {
        /**
         * @var MongoDB[] A list of available database configurations.
         */
        private static $databases = [];

        /**
         * Returns a MongoDB instance
         *
         * @param string $key The key that represents this MongoDB instance.
         *
         * @return MongoDB The MongoDB instance for the given key, or null when none can be found.
         */
        public static function get(string $key = 'default')
        {
            return isset(static::$databases[$key]) ? static::$databases[$key] : null;
        }

        /**
         * Loads a set of MongoDB instances from config and sets the MongoDB instances by the key they were defined.
         *
         * @param $path string The path to the config file.
         */
        public static function load($path)
        {
            $config = (new Config)->get($path);

            foreach ($config as $key => $value) {
                $mongodb = new MongoDB();
                $mongodb->setHost($value->host);
                $mongodb->setPort($value->port);

                static::set($key, $mongodb);
            }
        }

        /**
         * Sets a MongoDB instance
         *
         * @param string $key The key that represents this MongoDB instance.
         * @param MongoDB $value The MongoDB instance for the given key.
         */
        public static function set(string $key, MongoDB $value)
        {
            static::$databases[$key] = $value;
        }
    }